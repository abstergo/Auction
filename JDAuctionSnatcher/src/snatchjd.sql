/*
Navicat MySQL Data Transfer

Source Server         : mysql_local
Source Server Version : 50151
Source Host           : localhost:3306
Source Database       : snatchjd

Target Server Type    : MYSQL
Target Server Version : 50151
File Encoding         : 65001

Date: 2015-03-09 19:23:48
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `his_dealdetail`
-- ----------------------------
DROP TABLE IF EXISTS `his_dealdetail`;
CREATE TABLE `his_dealdetail` (
  `goodsclassname` varchar(100) DEFAULT NULL,
  `goodsid` varchar(20) NOT NULL,
  `goodsname` varchar(100) NOT NULL,
  `attachlist` varchar(2000) DEFAULT NULL,
  `jdprice` double(20,8) DEFAULT NULL,
  `place` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `dealprice` double(20,8) NOT NULL,
  `dealuser` varchar(50) DEFAULT NULL,
  `starttime` datetime DEFAULT NULL,
  `dealdatetime` datetime NOT NULL,
  `ts` datetime NOT NULL,
  PRIMARY KEY (`goodsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `waiting_id`
-- ----------------------------
DROP TABLE IF EXISTS `waiting_id`;
CREATE TABLE `waiting_id` (
  `goodsid` varchar(20) NOT NULL,
  `ts` datetime NOT NULL,
  PRIMARY KEY (`goodsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
