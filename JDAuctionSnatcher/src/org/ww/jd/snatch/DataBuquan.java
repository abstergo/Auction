package org.ww.jd.snatch;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.ww.jd.auction.GoodsInfo;
import org.ww.jd.auction.UnSessionUtils;

public class DataBuquan {
	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
		}
	}

	public static void main(String[] args) throws SQLException {
		DataBuquan buquan = new DataBuquan();
		List<HisDealDetail> res = buquan.excuteQuery("starttime is null");
		for (HisDealDetail hisDealDetail : res) {
			GoodsInfo goodsInfo = UnSessionUtils.getGoods(hisDealDetail.getGoodsid());
			hisDealDetail.setPlace(goodsInfo.getPlace());
			hisDealDetail.setStatus(goodsInfo.getStatus());
			hisDealDetail.setStarttime(new Timestamp(goodsInfo.getStartTimeMili()));
			buquan.updateDetail(hisDealDetail);
		}
		System.out.println(res.size());
	}

	private QueryRunner runner = new QueryRunner();

	private Connection createConnection() throws SQLException {

		String url = "jdbc:mysql://localhost:3306/snatchjd?user=root&password=root";
		Connection conn = DriverManager.getConnection(url);
		return conn;
	}

	public List<HisDealDetail> excuteQuery(String where) throws SQLException {
		String sql = "select * from his_dealdetail";
		if (where != null && !where.isEmpty()) {
			sql = sql + " where " + where;
		}
		ResultSetHandler<List<HisDealDetail>> rsh = new BeanListHandler<HisDealDetail>(
				HisDealDetail.class);
		try (Connection conn = createConnection()) {
			return runner.query(conn, sql, rsh);
		}
	}

	public void updateDetail(HisDealDetail detail) throws SQLException {
		String sql = "update his_dealdetail set status=?,place=?,starttime=? where goodsid=?";

		try (Connection conn = createConnection()) {
			runner.update(conn, sql,

			detail.getStatus(), detail.getPlace(), detail.getStarttime(),
					detail.getGoodsid());
		}
	}
}
