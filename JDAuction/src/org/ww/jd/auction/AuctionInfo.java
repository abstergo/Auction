package org.ww.jd.auction;

public class AuctionInfo {
	private String goodsId;
	private String operator;
	private String time;
	private String myPrice;

	public AuctionInfo(String goodsId, String operator, String time,
			String myPrice) {
		super();
		this.goodsId = goodsId;
		this.operator = operator;
		this.time = time;
		this.myPrice = myPrice;
	}

	/**
	 * @return the goodsId
	 */
	public String getGoodsId() {
		return goodsId;
	}

	/**
	 * @return the operator
	 */
	public String getOperator() {
		return operator;
	}

	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @return the myPrice
	 */
	public String getMyPrice() {
		return myPrice;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AuctionInfo [goodsId=" + goodsId + ", operator=" + operator
				+ ", time=" + time + ", myPrice=" + myPrice + "]";
	}

}
