package org.ww.jd.auction;

public class AuctionConst
{
  public static final String STATUS_RUNNING = "进行中";
  public static final String STATUS_END = "已结束";
  public static final String STATUS_GIVEUP = "已放弃";
  public static final String STATUS_NOSTART = "未开始";
  public static final int NAME_INX = 1;
  public static final int CURPRICE_INX = 2;
  public static final int JDPRICE_INX = 3;
  public static final int MYPRICE_INX = 4;
  public static final int PAIMAISTATUS_INX = 5;
  public static final int GOODSSTATUS_INX = 6;
  public static final int PLACE_INX = 7;
  public static final int STARTTIME_INX = 8;
  public static final int ENDTIME_INX = 9;
  public static final int ELSETIME_INX = 10;
  public static final int VMEMO_INX = 11;
  public static final int SETTING_INX = 12;
}