package org.ww.jd.auction;

/*    */import java.sql.Date;
import java.text.SimpleDateFormat;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HtmlUtils {
	public static void main(String[] args) throws JSONException {
		String scripts = "var dealModel={init_price:1.0,startTimeMili:1397102100000,endTimeMili:1397107502000};         dealModel['auctionStatus']=1;";

		int start = scripts.indexOf("dealModel=");
		int end = scripts.indexOf("};", start + 1);
		String starttime = scripts.substring(start, end + 1).replaceAll(
				"dealModel=", "");
		JSONObject dealModel = new JSONObject(starttime);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println(sdf.format(new Date(dealModel
				.getLong("startTimeMili"))));
		System.out
				.println(sdf.format(new Date(dealModel.getLong("endTimeMili"))));
	}

	public static GoodsInfo convertGoodsInfo(String goodsid, Element goodsNode) {
		GoodsInfo goodsInfo = new GoodsInfo(goodsid);
		String goodsName = getFirstElements(goodsNode, "h1").ownText();
		Double cur_price = Double.valueOf(Double.parseDouble(goodsNode
				.getElementById("cur_price").ownText().replaceAll("￥", "")));
		String baojia = getFirstElements(goodsNode, "del").ownText()
				.replaceAll("￥", "");
		try {
			Double jdPrice = Double.parseDouble(baojia);
			goodsInfo.setJDPrice(jdPrice);
		} catch (NumberFormatException e) {
			goodsInfo.setJDPrice(-1.0);
		}
		String status = getOwnText(goodsNode, "class", "fore9").replaceAll(
				"使用状态：", "");
		String place = getOwnText(goodsNode, "class", "fore2").replaceAll(
				"所在地：", "");
		goodsInfo.setGoodsName(goodsName);
		goodsInfo.setCurrentPrice(cur_price);
		goodsInfo.setStatus(status);
		goodsInfo.setPlace(place);
		
		return goodsInfo;
	}

	private static String getOwnText(Element goodsNode, String key, String value) {
		return goodsNode.getElementsByAttributeValue(key, value).get(0)
				.ownText();
	}

	public static Element getFirstElements(Element element, String tagName) {
		Elements elements = element.getElementsByTag(tagName);
		if (elements.size() == 0) {
			throw new RuntimeException("element");
		}
		return elements.get(0);
	}
}
