package org.ww.jd;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Image;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import org.ww.jd.auction.AuctionConst;
import org.ww.jd.auction.AuctionInfo;
import org.ww.jd.auction.GoodsDialog;
import org.ww.jd.auction.GoodsInfo;
import org.ww.jd.auction.IgnorException;
import org.ww.jd.auction.ServerTimeUtil;
import org.ww.jd.auction.SettingDialog;
import org.ww.jd.auction.StringUtils;
import org.ww.jd.auction.UnSessionUtils;
import org.ww.jd.goods.AuthCodeListener;
import org.ww.jd.goods.JDSession;
import org.ww.jd.log.Logger;
import org.ww.jd.utils.MusicPlay;
import org.ww.mail.MailSenderUtil;

public class AuctionSwingUI extends JFrame implements ActionListener {
	private static final long serialVersionUID = 4508432367320122215L;
	private static List<GoodsInfo> goodsInfos = new CopyOnWriteArrayList<>();
	private static List<String> overGoodsids = new CopyOnWriteArrayList<>();
	private Map<String, Integer> mapRowNo = new ConcurrentHashMap<>();
	private int workTime = 30 * 60 * 1000;
	// private ScheduledExecutorService threadManager;
	private DefaultTableModel model;
	private MenuItem addGoods;
	private MenuItem batchAddGoods;
	private MenuItem editGoods;
	private MenuItem giveupGoods;
	private MenuItem refreshGoods;
	private MenuItem importGoods;
	private MenuItem exportUnFinishGoods;

	private MenuItem auctionSetting;

	private JTable goodsInfoTable;

	public static void main(String[] args) {
		try {
			for (UIManager.LookAndFeelInfo info : UIManager
					.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception localException) {
		}
		final AuctionSwingUI auction = new AuctionSwingUI();
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

			@Override
			public void run() {
				auction.shutdown();
			}
		}));

		if (0 != Login.createAndShowGUI(auction)) {
			System.exit(0);
		}
		String userName = Login.usernameField.getText();
		@SuppressWarnings("deprecation")
		String password = Login.passwordField.getText();
		Setting.setUserName(userName);
		Setting.setPassword(password);
		boolean login = auction.login();
		if (!login) {
			JOptionPane.showMessageDialog(auction, "登录失败!");
			auction.onExportUnFinishGoods();
		}

		// if (login) {
		// auction.startMonitor();
		// }
		auction.startRefreshGoods();
		// if (args.length > 0) {
		// auction.doMyselfGoods(args[0]);
		// }
	}

	private JDSession jd = null;

	public AuctionSwingUI() {
		jd = new JDSession();
		initUI();
	}

	public boolean login() {

		return jd.login(new AuthCodeListener() {
			@Override
			public String authCode(String imgUrl) {
				return processAuthCode(imgUrl);
			}
		});

	}

	public void shutdown() {
		jd.closeSession();
		UnSessionUtils.closeHttp();
		SystemTray systemTray = SystemTray.getSystemTray();
		systemTray.remove(trayicon);
	}

	private String processAuthCode(String authCodeUrl) {
		Object authCodeObj = null;
		try {
			ImageIcon icon = new ImageIcon(new URL(authCodeUrl));
			authCodeObj = JOptionPane.showInputDialog(this, "请输入验证码", "",
					JOptionPane.INFORMATION_MESSAGE, icon, null, null);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		if (authCodeObj == null) {
			return processAuthCode(authCodeUrl);
		}
		return authCodeObj.toString();
	}

	public void doMyselfGoods(String keyword) {
		List<GoodsInfo> goodsList = UnSessionUtils
				.searchAuction(false, keyword);
		if (goodsList.isEmpty()) {
			System.out.println("木有");
			return;
		}
		System.out.println("总数:" + goodsList.size());
		for (GoodsInfo info : goodsList) {
			double price = info.getJDPriceDouble();
			if (price == -1) {
				continue;
			}
			if (price < 200) {
				continue;
			}
			if (info.getGoodsName().contains("蓝牙")) {
				addGoods(info.getGoodsId(), (int) (price * 0.1), 3);
			}

		}
	}

	TrayIcon trayicon;

	@SuppressWarnings("serial")
	private void initUI() {
		initSysTray();
		this.model = new DefaultTableModel() {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		this.model.setColumnIdentifiers(new String[] { "编号", "名称", "当前价格",
				"京东价", "我的价格", "拍卖状态", "商品状态", "所在地", "开始时间", "结束时间", "剩余时间",
				"备注", "最高价/加价" });
		this.goodsInfoTable = new JTable();
		this.goodsInfoTable.setDefaultRenderer(Object.class,
				createTableCellRenderer());
		goodsInfoTable.getSelectionModel().setSelectionMode(
				ListSelectionModel.SINGLE_SELECTION);
		this.goodsInfoTable.setModel(this.model);

		this.goodsInfoTable.setRowHeight(25);
		this.goodsInfoTable.getColumnModel()
				.getColumn(AuctionConst.STARTTIME_INX).setPreferredWidth(120);
		this.goodsInfoTable.getColumnModel()
				.getColumn(AuctionConst.ENDTIME_INX).setPreferredWidth(120);
		this.goodsInfoTable.getColumnModel().getColumn(AuctionConst.VMEMO_INX)
				.setPreferredWidth(150);
		this.goodsInfoTable.getColumnModel()
				.getColumn(AuctionConst.ELSETIME_INX).setPreferredWidth(100);
		this.goodsInfoTable.getColumnModel().getColumn(AuctionConst.NAME_INX)
				.setPreferredWidth(120);
		this.goodsInfoTable.getColumnModel()
				.getColumn(AuctionConst.MYPRICE_INX).setPreferredWidth(60);
		this.goodsInfoTable.getColumnModel()
				.getColumn(AuctionConst.JDPRICE_INX).setPreferredWidth(60);
		this.goodsInfoTable.getColumnModel()
				.getColumn(AuctionConst.CURPRICE_INX).setPreferredWidth(60);
		this.goodsInfoTable.getColumnModel().getColumn(AuctionConst.PLACE_INX)
				.setPreferredWidth(60);
		this.goodsInfoTable.getColumnModel().getColumn(0).setPreferredWidth(60);

		this.goodsInfoTable.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getButton() != 1) {
					return;
				}
				if (e.getClickCount() == 2) {
					int selectRow = AuctionSwingUI.this.goodsInfoTable
							.getSelectedRow();
					if (selectRow < 0) {
						return;
					}
					String goodsNo = Objects.toString(AuctionSwingUI.this.model
							.getValueAt(selectRow, 0));
					try {
						Desktop.getDesktop().browse(
								new URI("http://auction.jd.com/detail/"
										+ goodsNo));
					} catch (IOException iee) {
						iee.printStackTrace();
					} catch (URISyntaxException localURISyntaxException) {
					}
				}
			}
		});
		setTitle("我抢");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLayout(new BorderLayout());

		Menu goodsManage = new Menu("商品管理");

		this.addGoods = new MenuItem("增加商品");
		this.addGoods.addActionListener(this);
		goodsManage.add(this.addGoods);

		this.batchAddGoods = new MenuItem("批量增加商品");
		this.batchAddGoods.addActionListener(this);
		goodsManage.add(this.batchAddGoods);

		this.editGoods = new MenuItem("修改商品");
		this.editGoods.addActionListener(this);
		goodsManage.add(this.editGoods);

		this.giveupGoods = new MenuItem("放弃商品");
		this.giveupGoods.addActionListener(this);
		goodsManage.add(this.giveupGoods);

		this.refreshGoods = new MenuItem("刷新商品");
		this.refreshGoods.addActionListener(this);
		goodsManage.add(this.refreshGoods);

		Menu tools = new Menu("工具");
		this.importGoods = new MenuItem("导入");
		this.importGoods.addActionListener(this);
		tools.add(this.importGoods);

		this.exportUnFinishGoods = new MenuItem("导出未完成");
		this.exportUnFinishGoods.addActionListener(this);
		tools.add(this.exportUnFinishGoods);

		Menu setting = new Menu("设置");
		this.auctionSetting = new MenuItem("抢拍设置");
		this.auctionSetting.addActionListener(this);
		setting.add(this.auctionSetting);

		MenuBar menuBar = new MenuBar();
		menuBar.add(goodsManage);
		menuBar.add(tools);
		menuBar.add(setting);
		setMenuBar(menuBar);
		add(new JScrollPane(this.goodsInfoTable), "Center");
		setExtendedState(6);
		setVisible(true);
	}

	private void initSysTray() {
		Image image = Toolkit.getDefaultToolkit().getImage(
				getClass().getResource("/logo.png"));
		trayicon = new TrayIcon(image, "夺宝岛神器", createMenu());
		trayicon.setImageAutoSize(true);
		trayicon.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setState(JFrame.NORMAL);
				setVisible(true);
				toFront();

			}
		});
		try {
			SystemTray systemTray = SystemTray.getSystemTray();
			systemTray.add(trayicon);
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	private PopupMenu createMenu() {
		PopupMenu menu = new PopupMenu();
		MenuItem exit = new MenuItem("退出");
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ex) {
				System.exit(0);
			}

		});
		menu.add(exit);
		return menu;
	}

	public void addGoods(String goodsId, Integer maxPrice, Integer addPrice) {
		if (mapRowNo.containsKey(goodsId)) {
			throw new IllegalArgumentException("该商品已经存在，请不要重复添加");
		}
		try {
			GoodsInfo goodsInfo = UnSessionUtils.getGoods(goodsId);
			goodsInfos.add(goodsInfo);
			int rowNO = Integer.valueOf(goodsInfos.size() - 1);
			this.mapRowNo.put(goodsId, rowNO);

			this.model.addRow(new String[] { goodsId });
			int rowno = updateGoodsInfo(goodsInfo);
			editGoods(rowno, goodsId, maxPrice, addPrice);
			goodsInfoTable.getSelectionModel().setSelectionInterval(rowNO,
					rowNO);
			goodsInfoTable.repaint();
		} catch (Throwable e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
	}

	private int updateGoodsInfo(GoodsInfo goodsInfo) {
		int rowno = ((Integer) this.mapRowNo.get(goodsInfo.getGoodsId()))
				.intValue();
		this.model.setValueAt(goodsInfo.getGoodsName(), rowno,
				AuctionConst.NAME_INX);
		this.model.setValueAt(goodsInfo.getJDPriceString(), rowno,
				AuctionConst.JDPRICE_INX);
		this.model.setValueAt(goodsInfo.getPlace(), rowno,
				AuctionConst.PLACE_INX);
		this.model.setValueAt(goodsInfo.getStatus(), rowno,
				AuctionConst.GOODSSTATUS_INX);
		this.model.setValueAt(StringUtils.formatDateTime(goodsInfo
				.getStartTimeMili().longValue()), rowno,
				AuctionConst.STARTTIME_INX);
		this.model
				.setValueAt(StringUtils.formatDateTime(goodsInfo
						.getEndTimeMili().longValue()), rowno,
						AuctionConst.ENDTIME_INX);
		return rowno;
	}

	private void editGoods(int selectRow, String goodsNo, Integer maxPrice,
			Integer addPrice) {
		this.model.setValueAt(maxPrice + "/" + addPrice, selectRow,
				AuctionConst.SETTING_INX);
		Setting.setMaxPrice(goodsNo, maxPrice);
		Setting.setAddPrice(goodsNo, addPrice);
	}

	@SuppressWarnings("serial")
	private DefaultTableCellRenderer createTableCellRenderer() {
		return new DefaultTableCellRenderer() {
			public Component getTableCellRendererComponent(JTable table,
					Object value, boolean isSelected, boolean hasFocus,
					int row, int column) {
				Component comp = super.getTableCellRendererComponent(table,
						value, isSelected, hasFocus, row, column);

				Object status = Objects.toString(AuctionSwingUI.this.model
						.getValueAt(row, AuctionConst.PAIMAISTATUS_INX));

				if (AuctionConst.STATUS_END.equals(status)) {
					comp.setBackground(Color.GRAY);
				} else if (AuctionConst.STATUS_NOSTART.equals(status)) {
					comp.setBackground(Color.ORANGE);
				} else if (AuctionConst.STATUS_RUNNING.equals(status)) {
					comp.setBackground(Color.GREEN);
				} else if (AuctionConst.STATUS_GIVEUP.equals(status)) {
					comp.setBackground(Color.RED);
				}
				if (isSelected) {
					comp.setBackground(comp.getBackground().darker());
				}

				comp.setFont(new Font(comp.getFont().getFontName(), Font.BOLD,
						comp.getFont().getSize()));

				return comp;
			}
		};
	}

	public void startRefreshGoods() {
		// this.threadManager = Executors.newScheduledThreadPool(3);
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {
						Thread.sleep(Setting.getRateTime());
					} catch (InterruptedException e) {
						Thread.interrupted();
					}

					for (GoodsInfo goodsInfo : AuctionSwingUI.goodsInfos) {
						if (AuctionSwingUI.overGoodsids.contains(goodsInfo
								.getGoodsId()))
							continue;
						try {
							if (!AuctionSwingUI.this.gogogo(goodsInfo)) {
								AuctionSwingUI.overGoodsids.add(goodsInfo
										.getGoodsId());
							}
							AuctionSwingUI.this.goodsInfoTable.repaint();
						} catch (IgnorException localIgnorException) {
						}
					}

				}
			}
		}).start();
	}

	public void startMonitor() {
		// this.threadManager = Executors.newScheduledThreadPool(3);
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {
						Thread.sleep(Setting.getRateTime());
					} catch (InterruptedException e) {
						Thread.interrupted();
					}
					List<AuctionInfo> hasGoods = jd.getAuctionResult();
					// for (AuctionInfo auctionInfo : hasGoods) {
					// System.out.println(auctionInfo);
					// }

					for (AuctionInfo auctionInfo : hasGoods) {
						if (auctionInfo.getOperator().contains("转成订单")) {
							successExit(auctionInfo);
							return;
						}
						for (GoodsInfo goodsInfo : AuctionSwingUI.goodsInfos) {
							if (goodsInfo.getGoodsId().equals(
									auctionInfo.getGoodsId())) {
								successExit(auctionInfo);
								return;
							}
						}
					}
				}
			}

		}).start();
	}

	private void successExit(AuctionInfo auctionInfo) {
		// System.out.println(auctionInfo);
		// MailSenderUtil.send("【！！！！！】夺宝岛" + auctionInfo,
		// "http://auction.jd.com/detail/" + auctionInfo.getGoodsId());
		// System.exit(0);
	}

	private boolean rest(long lefttime) {
		return lefttime <= this.workTime;
	}

	private boolean gogogo(GoodsInfo goodsInfo) {
		if (goodsInfo != null) {
			String goodsid = goodsInfo.getGoodsId();
			int rowno = ((Integer) this.mapRowNo.get(goodsid)).intValue();
			// Double curPrice;
			// try {
			// curPrice = UnSessionUtils.getCurrentPrice(goodsid);
			// } catch (Exception e) {
			// Logger.error(e);
			// curPrice = Double.valueOf(1.0D);
			// }
			// goodsInfo.setCurrentPrice(curPrice);
			// this.model.setValueAt(curPrice, rowno,
			// AuctionConst.CURPRICE_INX);

			long start = goodsInfo.getStartTimeMili().longValue();
			long end = goodsInfo.getEndTimeMili().longValue();
			long serverTime = ServerTimeUtil.getServerTime(goodsid).longValue();
			int maxPrice = Setting.maxPrice(goodsid);
			if (maxPrice < 2) {
				this.model.setValueAt("主动放弃", rowno, AuctionConst.VMEMO_INX);
				this.model.setValueAt(AuctionConst.STATUS_GIVEUP, rowno,
						AuctionConst.PAIMAISTATUS_INX);
				return true;
			}

			if (start > serverTime) {

				this.model.setValueAt(AuctionConst.STATUS_NOSTART, rowno,
						AuctionConst.PAIMAISTATUS_INX);
				this.model.setValueAt(
						StringUtils.formartTime(start - serverTime), rowno,
						AuctionConst.ELSETIME_INX);
				return true;
			}
			if (end < serverTime) {
				this.model.setValueAt(AuctionConst.STATUS_END, rowno,
						AuctionConst.PAIMAISTATUS_INX);
				return false;
			}

			long lefttime = end - serverTime;
			this.model.setValueAt(StringUtils.formartTime(lefttime), rowno,
					AuctionConst.ELSETIME_INX);
			if (!rest(lefttime)) {
				this.model.setValueAt(AuctionConst.STATUS_RUNNING, rowno,
						AuctionConst.PAIMAISTATUS_INX);
				this.model.setValueAt(Double.valueOf(1.0D), rowno,
						AuctionConst.CURPRICE_INX);
				this.model.setValueAt(
						"太早了,休息"
								+ StringUtils.formartTime(lefttime
										- this.workTime) + "后干活", rowno,
						AuctionConst.VMEMO_INX);
				return true;
			}
			Double curPrice;
			try {
				curPrice = UnSessionUtils.getCurrentPrice(goodsid);
			} catch (Exception e) {
				Logger.error(e);
				curPrice = Double.valueOf(1.0D);
			}
			goodsInfo.setCurrentPrice(curPrice);
			this.model.setValueAt(curPrice, rowno, AuctionConst.CURPRICE_INX);

			this.model.setValueAt("", rowno, AuctionConst.VMEMO_INX);
			if (curPrice >= maxPrice) {
				this.model.setValueAt("有人出到" + curPrice + "了,放弃", rowno,
						AuctionConst.VMEMO_INX);
				this.model.setValueAt(AuctionConst.STATUS_GIVEUP, rowno,
						AuctionConst.PAIMAISTATUS_INX);
				return true;
			}
			this.model.setValueAt(AuctionConst.STATUS_RUNNING, rowno,
					AuctionConst.PAIMAISTATUS_INX);
			if (lefttime < Setting.doPriceTime()) {
				int newPrice = curPrice.intValue() + Setting.addPrice(goodsid);
				if (newPrice > maxPrice) {
					newPrice = maxPrice;
				}
				boolean success = jd.doPrice(goodsInfo, newPrice);
				// String url = UnSessionUtils.doPriceWithBrowser(goodsInfo,
				// newPrice);
				this.model.setValueAt(Integer.valueOf(newPrice), rowno,
						AuctionConst.MYPRICE_INX);
				this.model.setValueAt("出价:" + newPrice + "元，祝我成功吧", rowno,
						AuctionConst.VMEMO_INX);
				this.model.setValueAt(AuctionConst.STATUS_END, rowno,
						AuctionConst.PAIMAISTATUS_INX);
				if (success) {
					notice(goodsInfo, newPrice);
				}

				return false;
			}
			return true;
		}
		return false;
	}

	private MusicPlay myMusicPlay = null;

	private void notice(GoodsInfo goodsInfo, int newPrice) {
		String title = "夺宝岛：" + goodsInfo.getGoodsName() + "-" + newPrice
				+ " 元";
		MailSenderUtil.send(title,
				"http://auction.jd.com/detail/" + goodsInfo.getGoodsId()
						+ "\n from:" + Setting.getUserName());
		trayicon.displayMessage(title, goodsInfo.getGoodsId(), MessageType.INFO);
		if (myMusicPlay == null) {
			myMusicPlay = new MusicPlay("/music/dog.wav");
		}
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					myMusicPlay.continuousStart();
					Thread.sleep(3000);
					myMusicPlay.continuousStop();
				} catch (Exception e) {
					Logger.error(e);
				}

			}
		});

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.addGoods) {
			onAddGoods();
		} else if (e.getSource() == this.editGoods) {
			onEditGoods();
		} else if (e.getSource() == this.giveupGoods) {
			onGiveupGoods();
		} else if (e.getSource() == this.refreshGoods) {
			onRefreshGoods();
		} else if (e.getSource() == auctionSetting) {
			onSetting();
		} else if (e.getSource() == batchAddGoods) {
			onBatchAddGoods();
		} else if (e.getSource() == importGoods) {
			onImportGoods();
		} else if (e.getSource() == exportUnFinishGoods) {
			onExportUnFinishGoods();
		}
	}

	private void onExportUnFinishGoods() {
		JFileChooser jfc = createFileChooser();
		if (JFileChooser.APPROVE_OPTION == jfc.showSaveDialog(this)) {
			File file = jfc.getSelectedFile();
			if (file.exists()) {
				file.delete();
			}
			if (!file.getName().toLowerCase().endsWith(".jd")) {
				file = new File(file.getAbsolutePath() + ".jd");
			}

			StringBuilder txt = new StringBuilder();
			for (GoodsInfo goodsInfo : AuctionSwingUI.goodsInfos) {
				if (AuctionSwingUI.overGoodsids
						.contains(goodsInfo.getGoodsId())) {
					continue;
				}
				txt.append(goodsInfo.getGoodsId());
				txt.append("-");
				txt.append(Setting.maxPrice(goodsInfo.getGoodsId()));
				txt.append("-");
				txt.append(Setting.addPrice(goodsInfo.getGoodsId()));
				txt.append(",");
			}
			if (txt.length() > 0) {
				txt.deleteCharAt(txt.length() - 1);
			}

			String content = txt.toString();
			writeFile(file, content);
		}

	}

	private JFileChooser createFileChooser() {
		JFileChooser jfc = new JFileChooser();
		jfc.setAcceptAllFileFilterUsed(false);
		jfc.setFileFilter(new FileFilter() {

			@Override
			public String getDescription() {
				return "夺宝岛";
			}

			@Override
			public boolean accept(File f) {
				return (f.getName().endsWith(".jd"));
			}
		});
		return jfc;
	}

	private void writeFile(File file, String content) {
		FileWriter fw = null;
		try {
			fw = new FileWriter(file);
			fw.write(content);
		} catch (IOException e) {
		} finally {
			try {
				if (fw != null) {
					fw.close();
				}
			} catch (IOException e) {
			}
		}
	}

	private void onImportGoods() {
		JFileChooser jfc = createFileChooser();
		if (JFileChooser.APPROVE_OPTION == jfc.showOpenDialog(this)) {
			File file = jfc.getSelectedFile();
			String content = readFile(file);
			String[] ids = content.split(",");
			if (ids == null || ids.length == 0) {
				JOptionPane.showMessageDialog(this, "木有");
				return;
			}
			for (String string : ids) {
				if (string.trim().length() == 0) {
					continue;
				}
				String[] goodSettings = string.split("-");
				if (goodSettings.length < 3) {
					continue;
				}
				addGoods(goodSettings[0], Integer.valueOf(goodSettings[1]),
						Integer.valueOf(goodSettings[2]));
			}
			System.out.println(content);
		}

	}

	private String readFile(File file) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String line;
			StringBuilder txt = new StringBuilder();
			while ((line = br.readLine()) != null) {
				txt.append(line);
			}

			String content = txt.toString();
			return content;
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void onBatchAddGoods() {
		String keyword = JOptionPane.showInputDialog(this, "输入关键字(30%)");
		while (keyword == null || keyword.trim().length() == 0) {
			keyword = JOptionPane.showInputDialog(this, "请输入关键字(30%)");
		}
		List<GoodsInfo> list = UnSessionUtils.searchAuction(false, keyword);
		if (list.isEmpty()) {
			JOptionPane.showMessageDialog(this, "木有");
			return;
		}
		int addSize = 0;
		for (GoodsInfo goodsInfo : list) {
			if (mapRowNo.containsKey(goodsInfo.getGoodsId())) {
				continue;
			}
			try {
				int maxPrice = (int) (goodsInfo.getJDPriceDouble() * 0.3);
				if (maxPrice > 300) {
					maxPrice = 300;
				}
				addGoods(goodsInfo.getGoodsId(), maxPrice, 2);
				addSize++;
			} catch (Throwable te) {

			}
		}

		JOptionPane.showMessageDialog(this, "加了：" + addSize + "条商品");
	}

	private void onRefreshGoods() {
		for (GoodsInfo goods : goodsInfos) {
			GoodsInfo newGoodsInfo = UnSessionUtils
					.getGoods(goods.getGoodsId());
			goods.update(newGoodsInfo);
			updateGoodsInfo(goods);
		}

	}

	private void onGiveupGoods() {
		int selectRow = this.goodsInfoTable.getSelectedRow();
		if (selectRow < 0) {
			JOptionPane.showMessageDialog(this, "请选择要修改的商品");
			return;
		}
		String goodsNo = Objects.toString(this.model.getValueAt(selectRow, 0));
		String status = Objects.toString(this.model.getValueAt(selectRow,
				AuctionConst.PAIMAISTATUS_INX));
		if (AuctionConst.STATUS_END.equals(status)) {
			JOptionPane.showMessageDialog(this, "该拍卖已经结束");
			return;
		}
		editGoods(selectRow, goodsNo, 1, 1);

	}

	private void onSetting() {
		SettingDialog dialog = new SettingDialog(this);
		dialog.setDoPriceTime(Setting.doPriceTime());
		dialog.setRateTime(Setting.getRateTime());
		dialog.setVisible(true);
		if (dialog.isOk()) {
			Setting.setDoPriceTime(dialog.getDoPriceTime());
			Setting.setRateTime(dialog.getRateTime());
		}

	}

	private void onEditGoods() {
		int selectRow = this.goodsInfoTable.getSelectedRow();
		if (selectRow < 0) {
			JOptionPane.showMessageDialog(this, "请选择要修改的商品");
			return;
		}
		String goodsNo = Objects.toString(this.model.getValueAt(selectRow, 0));
		String status = Objects.toString(this.model.getValueAt(selectRow,
				AuctionConst.PAIMAISTATUS_INX));
		if (AuctionConst.STATUS_END.equals(status)) {
			JOptionPane.showMessageDialog(this, "该拍卖已经结束");
			return;
		}
		Integer maxPrice = Integer.valueOf(Setting.maxPrice(goodsNo));
		Integer addPrice = Integer.valueOf(Setting.addPrice(goodsNo));
		GoodsDialog dialog = new GoodsDialog(this);
		dialog.setStateEdit();
		dialog.setGoodsNo(goodsNo);
		dialog.setMaxPrice(maxPrice);
		dialog.setAddPrice(addPrice);
		dialog.setVisible(true);
		if (dialog.isOk()) {
			editGoods(selectRow, goodsNo, dialog.getMaxPrice(),
					dialog.getAddPrice());
		}
	}

	private void onAddGoods() {
		GoodsDialog dialog = new GoodsDialog(this);
		dialog.setStateAdd();
		dialog.setVisible(true);
		if (dialog.isOk())
			try {
				addGoods(dialog.getGoodsNo(), dialog.getMaxPrice(),
						dialog.getAddPrice());
			} catch (Exception e) {
				JOptionPane.showMessageDialog(this, e.getMessage());
			}
	}
}