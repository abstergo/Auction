package org.ww.jd;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

public class Setting {
	private static Map<String, Integer> maxPriceConfig = new ConcurrentHashMap<>();
	private static Map<String, Integer> addPriceConfig = new ConcurrentHashMap<>();
	private static long rateTime = 1000L;
	private static long doPriceTime = 5000L;
	
	private static String userName ;
	private static String password;

	static {
		Properties prop = new Properties();
		try {
			prop.load(Setting.class
					.getResourceAsStream("/setting.properties"));
			rateTime = Integer.valueOf(prop.getProperty("rateTime")).intValue();
			doPriceTime = Integer.valueOf(prop.getProperty("doPriceTime"))
					.intValue();
//			userName = prop.getProperty("userName");
//			password = prop.getProperty("password");
			System.out.println("rateTime(扫描时间)=" + rateTime + " ms");
			System.out.println("doPriceTime(剩余多久出价)=" + doPriceTime + " ms");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public synchronized static long getRateTime() {
		return rateTime;
	}

	public static long doPriceTime() {
		return doPriceTime;
	}
	public static void setRateTime(long newTime){
		rateTime = newTime;
	}
	public static void setDoPriceTime(long newTime){
		doPriceTime = newTime;
	}

	public static void setUserName(String userName) {
		Setting.userName = userName;
		System.out.println("用户名=" + userName);
	}

	public static void setPassword(String password) {
		Setting.password = password;
	}

	/**
	 * @return the userName
	 */
	public static String getUserName() {
		return userName;
	}

	/**
	 * @return the password
	 */
	public static String getPassword() {
		return password;
	}

	public static int maxPrice(String goodsid) {
		if (!maxPriceConfig.containsKey(goodsid)) {
			return 10;
		}
		return ((Integer) maxPriceConfig.get(goodsid)).intValue();
	}

	public static int addPrice(String goodsid) {
		if (!addPriceConfig.containsKey(goodsid)) {
			return 1;
		}
		return ((Integer) addPriceConfig.get(goodsid)).intValue();
	}

	public static void setMaxPrice(String goodsid, Integer maxPrice) {
		maxPriceConfig.put(goodsid, maxPrice);
	}

	public static void setAddPrice(String goodsid, Integer addPrice) {
		addPriceConfig.put(goodsid, addPrice);
	}

	public static void removeMaxPrice(String goodsid) {
		maxPriceConfig.remove(goodsid);
	}

	public static void removeAddPrice(String goodsid) {
		addPriceConfig.remove(goodsid);
	}
}
