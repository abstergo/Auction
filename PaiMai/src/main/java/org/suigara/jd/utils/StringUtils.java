package org.suigara.jd.utils;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class StringUtils {
	static final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	public static String formatDateTime(long millinTime) {
		return sdf.format(new Date(millinTime));
	}

	public static String toString(Object obj) {
		if (obj == null) {
			return null;
		}
		return obj.toString();
	}
	

	public static String formatTime(long millinTime) {
		String resultStr = "";
		long seconds = millinTime / 1000L;
		int showseconds = (int) (seconds % 60L);
		int minutes = (int) (seconds / 60L);

		int showMinute = minutes % 60;
		int hour = minutes / 60;
		if (hour > 0) {
			resultStr = resultStr + hour + "Сʱ";
		}
		if (showMinute > 0) {
			resultStr = resultStr + showMinute + "��";
		}
		if (showseconds > 0) {
			resultStr = resultStr + showseconds + "��";
		}

		return resultStr;
	}
}
