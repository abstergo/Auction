package org.suigara.jd.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.suigara.jd.exception.IgnorException;

public class HttpClientUtils {
	public static Document doGetHtml(CloseableHttpClient httpclient, String url) {
		String content = doGetStringWithReferer(httpclient, url, null);
		return Jsoup.parse(content);
	}

	public static JSONObject doGetJson(CloseableHttpClient httpclient,
			String url, String curUrl) {
		String doGetString = doGetStringWithReferer(httpclient, url, curUrl);

		try {

			JSONObject jsobj = new JSONObject(doGetString);
			return jsobj;
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	public static JSONArray doGetJsonArray(CloseableHttpClient httpclient,
			String url) {
		String doGetString = doGetString(httpclient, url);
		try {
			JSONArray jsobj = new JSONArray(doGetString);
			return jsobj;
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	public static String doGetString(CloseableHttpClient httpclient, String url) {
		return doGetStringWithReferer(httpclient, url, null);
	}

	private static String doGetStringWithReferer(
			CloseableHttpClient httpclient, String url, String curUrl) {
		HttpGet httpGet = new HttpGet(url);
		// JDURLConsts.AUCTION_URL+"/"+goods.getGoodsId()
		if (curUrl != null) {
			httpGet.setHeader("Referer", curUrl);
		}
		CloseableHttpResponse response = null;
		try {
			response = httpclient.execute(httpGet);
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("errorcode="
						+ response.getStatusLine().getStatusCode());
			}
			HttpEntity httpEntity = response.getEntity();

			if (httpEntity == null) {
				throw new RuntimeException("httpEntity=null");
			}
			InputStream content = httpEntity.getContent();
			String str = getContentString(content);
			content.close();
			return str;
		} catch (IOException e) {
			throw new IgnorException(e);
		} finally {
			if (httpGet != null) {
				httpGet.abort();
			}
			if (response != null) {
				try {
					response.close();
				} catch (IOException localIOException2) {
				}
			}

		}
	}

	public static String getContentString(InputStream content) {
		StringBuilder entityStringBuilder = new StringBuilder();
		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(new InputStreamReader(content,
					"UTF-8"), 8192);
			String line = null;

			while ((line = bufferedReader.readLine()) != null) {
				entityStringBuilder.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
			try {
				if (bufferedReader != null)
					bufferedReader.close();
			} catch (IOException localIOException1) {
			}
		} finally {
			try {
				if (bufferedReader != null)
					bufferedReader.close();
			} catch (IOException localIOException2) {
			}
		}
		return entityStringBuilder.toString();
	}
}
