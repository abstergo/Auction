package org.suigara.jd.bean;

public class AutoAddVO {
	private final String keyword;
	private final double offrate;
	private final boolean wholeWord;

	public AutoAddVO(String keyword, double offrate) {
		this(keyword, offrate, false);
	}

	public AutoAddVO(String keyword, double offrate, boolean wholeWord) {
		super();
		this.keyword = keyword;
		this.offrate = offrate;
		this.wholeWord = wholeWord;
	}

	public String getKeyword() {
		return keyword;
	}

	public double getOffrate() {
		return offrate;
	}

	public boolean isWholeWord() {
		return wholeWord;
	}

}
