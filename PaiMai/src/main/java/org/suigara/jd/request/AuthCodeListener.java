package org.suigara.jd.request;

public interface AuthCodeListener {
	String authCode(String imgUrl);
}
