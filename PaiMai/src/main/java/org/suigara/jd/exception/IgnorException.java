package org.suigara.jd.exception;

public class IgnorException extends RuntimeException {
	private static final long serialVersionUID = -6531497913337053909L;

	public IgnorException(Throwable e) {
		super(e);
	}
}
