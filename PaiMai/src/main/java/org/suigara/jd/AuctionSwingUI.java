package org.suigara.jd;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.filechooser.FileFilter;

import me.chanjar.weixin.common.exception.WxErrorException;

import org.apache.http.conn.HttpHostConnectException;
import org.suigara.jd.bean.AuctionInfo;
import org.suigara.jd.bean.AutoAddVO;
import org.suigara.jd.bean.GoodsInfo;
import org.suigara.jd.bean.GoodsShortcut;
import org.suigara.jd.constants.AuctionConst;
import org.suigara.jd.context.GoodsContext;
import org.suigara.jd.exception.IgnorException;
import org.suigara.jd.log.Logger;
import org.suigara.jd.mail.MailSenderUtil;
import org.suigara.jd.request.AuthCodeListener;
import org.suigara.jd.request.JDSession;
import org.suigara.jd.request.UnSessionUtils;
import org.suigara.jd.ui.AutoAddGoodsDialog;
import org.suigara.jd.ui.DisplayTable;
import org.suigara.jd.ui.GoodsDialog;
import org.suigara.jd.ui.SettingDialog;
import org.suigara.jd.utils.MusicPlay;
import org.suigara.jd.utils.StringUtils;
import org.suigara.jd.utils.WeixinUtils;

public class AuctionSwingUI extends JFrame implements ActionListener {
	private static final int _1MINUTE = 60 * 1000;
	private static final long serialVersionUID = 4508432367320122215L;
	private List<String> overGoodsids = new CopyOnWriteArrayList<>();
	public static final File goodsFile = new File("last.jd");

	private int workTime = 30 * _1MINUTE;

	private MenuItem addGoods;
	private MenuItem batchAddGoods;
	private MenuItem editGoods;
	private MenuItem removeGoods;
	private MenuItem giveupGoods;
	private MenuItem refreshGoods;
	private MenuItem clearFinishGoods;
	private MenuItem importGoods;
	private MenuItem exportUnFinishGoods;
	private MenuItem keywordManage;

	private MenuItem auctionSetting;

	private DisplayTable goodsInfoTable;

	private List<AutoAddVO> autoAddGoods = new CopyOnWriteArrayList<>();
	{
//		autoAddGoods.add(new AutoAddVO("华为手环B2", 0.2));
//		autoAddGoods.add(new AutoAddVO("飞利浦 空气净化器  AC4076", 0.5));
//		autoAddGoods.add(new AutoAddVO("飞利浦  空气净化器 AC4372", 0.5));
//		autoAddGoods.add(new AutoAddVO("华为手环B2", 0.2));
//		autoAddGoods.add(new AutoAddVO("Fitbit Charge HR", 0.2));
//		autoAddGoods.add(new AutoAddVO("摩托罗拉 新一代 Moto 360", 0.12));
	}

	private JDSession jd = null;

	public AuctionSwingUI() {
		jd = new JDSession();
		initUI();
	}

	public boolean login() {

		return jd.login(new AuthCodeListener() {
			@Override
			public String authCode(String imgUrl) {
				return processAuthCode(imgUrl);
			}
		});

	}

	public void shutdown() {
		try {
			saveUnfinishGoods(goodsFile);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		try {
			jd.closeSession();
			UnSessionUtils.closeHttp();
		} catch (Throwable e) {

		}
		try {
			SystemTray systemTray = SystemTray.getSystemTray();
			systemTray.remove(trayicon);
		} catch (Throwable e) {

		}

		System.exit(0);
	}

	private String processAuthCode(String authCodeUrl) {
		Object authCodeObj = null;
		try {
			ImageIcon icon = new ImageIcon(new URL(authCodeUrl));
			authCodeObj = JOptionPane.showInputDialog(this, "请输入验证码", "",
					JOptionPane.INFORMATION_MESSAGE, icon, null, null);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		if (authCodeObj == null) {
			return processAuthCode(authCodeUrl);
		}
		return authCodeObj.toString();
	}

	TrayIcon trayicon;

	private void initUI() {
		initSysTray();
		setTitle("我抢");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLayout(new BorderLayout());
		initMenu();
		goodsInfoTable = new DisplayTable();
		add(new JScrollPane(this.goodsInfoTable), "Center");
		setExtendedState(6);
		setVisible(true);
	}

	private void initMenu() {
		Menu goodsManage = new Menu("商品管理");

		this.addGoods = new MenuItem("增加商品");
		this.addGoods.addActionListener(this);
		goodsManage.add(this.addGoods);

		this.batchAddGoods = new MenuItem("批量增加商品");
		this.batchAddGoods.addActionListener(this);
		goodsManage.add(this.batchAddGoods);

		this.editGoods = new MenuItem("修改商品");
		this.editGoods.addActionListener(this);
		goodsManage.add(this.editGoods);

		this.removeGoods = new MenuItem("删除商品");
		this.removeGoods.addActionListener(this);
		goodsManage.add(this.removeGoods);

		this.giveupGoods = new MenuItem("放弃商品");
		this.giveupGoods.addActionListener(this);
		goodsManage.add(this.giveupGoods);

		this.refreshGoods = new MenuItem("刷新商品");
		this.refreshGoods.addActionListener(this);
		goodsManage.add(this.refreshGoods);

		this.clearFinishGoods = new MenuItem("删除已完成");
		this.clearFinishGoods.addActionListener(this);
		goodsManage.add(this.clearFinishGoods);

		Menu tools = new Menu("工具");
		this.importGoods = new MenuItem("导入");
		this.importGoods.addActionListener(this);
		tools.add(this.importGoods);

		this.exportUnFinishGoods = new MenuItem("导出未完成");
		this.exportUnFinishGoods.addActionListener(this);
		tools.add(this.exportUnFinishGoods);

		this.keywordManage = new MenuItem("管理关键字");
		this.keywordManage.addActionListener(this);
		tools.add(this.keywordManage);

		Menu setting = new Menu("设置");
		this.auctionSetting = new MenuItem("抢拍设置");
		this.auctionSetting.addActionListener(this);
		setting.add(this.auctionSetting);

		MenuBar menuBar = new MenuBar();
		menuBar.add(goodsManage);
		menuBar.add(tools);
		menuBar.add(setting);
		setMenuBar(menuBar);
	}

	private void initSysTray() {
		Image image = Toolkit.getDefaultToolkit().getImage(
				getClass().getResource("/logo.png"));
		trayicon = new TrayIcon(image, "夺宝岛神器", createMenu());
		trayicon.setImageAutoSize(true);
		trayicon.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setState(JFrame.NORMAL);
				setVisible(true);
				toFront();

			}
		});
		try {
			SystemTray systemTray = SystemTray.getSystemTray();
			systemTray.add(trayicon);
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	private PopupMenu createMenu() {
		PopupMenu menu = new PopupMenu();
		MenuItem exit = new MenuItem("退出");
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ex) {
				shutdown();
			}

		});
		menu.add(exit);
		return menu;
	}

	public void startRefreshGoods() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					sleep(Setting.getRateTime());

					for (GoodsInfo goodsInfo : goodsInfoTable.getGoodsInfos()) {
						if (overGoodsids.contains(goodsInfo.getGoodsId()))
							continue;
						try {
							if (!AuctionSwingUI.this.gogogo(goodsInfo)) {
								overGoodsids.add(goodsInfo.getGoodsId());
							}
							AuctionSwingUI.this.goodsInfoTable.repaint();
						} catch (IgnorException localIgnorException) {
						}
					}

				}
			}
		}).start();
	}

	public void startAutoSearch() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					Logger.info("自动添加商品，关键字(" + autoAddGoods.size() + ")个");
					int i = 0;
					for (AutoAddVO autoAddVO : autoAddGoods) {
						i++;
						Logger.info(i + ":" + autoAddVO.getKeyword() + ","
								+ autoAddVO.getOffrate() + ","
								+ autoAddVO.isWholeWord());
					}
					try {
						for (AutoAddVO autoAddVO : autoAddGoods) {
							autoAddGoods(autoAddVO);
						}

					} catch (Exception e) {
						if (e instanceof IgnorException) {
							continue;
						}
						Logger.error(e);
					}

					// 30分钟刷一次
					sleep(30 * _1MINUTE);
				}
			}

		}).start();
	}

	/**
	 * 对于已经拍到的东西的最后提醒时间
	 */
	private Map<String, Long> noticeDeals = new HashMap<String, Long>();

	public void startMonitor() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {

					try {
						List<AuctionInfo> hasGoods = jd.getAuctionResult();
						Calendar instance = Calendar.getInstance();
						for (AuctionInfo auctionInfo : hasGoods) {
							String goodsId = auctionInfo.getGoodsId();
							if (noticeDeals.containsKey(goodsId)) {
								long time = instance.getTime().getTime()
										- noticeDeals.get(goodsId);
								// 十分钟提醒一次
								if (time < 10 * _1MINUTE) {
									continue;
								}
							}
							String description = "时间：" + auctionInfo.getTime()
									+ ",出价:" + auctionInfo.getMyPrice();
							try {
								WeixinUtils.sendWeiXin(
										auctionInfo.getGoodsName(),
										description, auctionInfo.getPicUrl(),
										auctionInfo.getUrl());
								noticeDeals.put(goodsId, instance.getTime()
										.getTime());
							} catch (WxErrorException e) {
								Logger.error("通知微信失败:" + e.getMessage());
							}

						}
						// 30秒刷一次
						sleep(30 * 1000);
					} catch (Exception e) {
						Logger.error(e);
						// 10秒刷一次
						sleep(10 * 1000);
					}

				}
			}

		}).start();
	}

	private void sleep(long mills) {
		try {
			Thread.sleep(mills);
		} catch (InterruptedException e) {
			Thread.interrupted();
		}
	}

	private boolean rest(long lefttime) {
		return lefttime <= this.workTime;
	}

	private boolean gogogo(GoodsInfo goodsInfo) {
		if (goodsInfo != null) {
			String goodsId = goodsInfo.getGoodsId();
			GoodsShortcut shotcut;
			try {
				shotcut = UnSessionUtils.getGoodsShortcut(goodsId);
			} catch (Exception e) {
				if(e instanceof IgnorException){
					return true;
				}
				if (!e.getMessage().contains("errorcode=503")
						&& !(e instanceof HttpHostConnectException)) {
					Logger.error(e);
				} else {
					Logger.error(e.getMessage());
				}
				return true;
			}

			int maxPrice = GoodsContext.getInstance().maxPrice(goodsId);

			if (shotcut.getAuctionStatus() == 0) {

				goodsInfoTable.updatePaimaiStatus(goodsId,
						AuctionConst.STATUS_NOSTART);
				goodsInfoTable.updateRemainTime(goodsId,
						shotcut.getRemainTime());
				return true;
			}
			if (shotcut.getAuctionStatus() == 2) {
				goodsInfoTable.updatePaimaiStatus(goodsId,
						AuctionConst.STATUS_END);
				return false;
			}
			if (maxPrice < 2) {
				goodsInfoTable.updateMemo(goodsId, "主动放弃");
				goodsInfoTable.updatePaimaiStatus(goodsId,
						AuctionConst.STATUS_GIVEUP);
				return true;
			}

			long lefttime = shotcut.getRemainTime();
			goodsInfoTable.updateRemainTime(goodsId, lefttime);
			if (!rest(lefttime)) {
				goodsInfoTable.updatePaimaiStatus(goodsId,
						AuctionConst.STATUS_RUNNING);
				goodsInfoTable
						.updateCurrentPrice(goodsId, Double.valueOf(1.0D));
				goodsInfoTable.updateMemo(
						goodsId,
						"太早了,休息"
								+ StringUtils.formatTime(lefttime
										- this.workTime) + "后干活");
				return true;
			}
			Double curPrice = shotcut.getCurrentPrice();

			goodsInfo.setCurrentPrice(curPrice);
			goodsInfoTable.updateCurrentPrice(goodsId, curPrice);
			goodsInfoTable.updateMemo(goodsId, "");
			if (curPrice >= maxPrice) {
				goodsInfoTable.updateMemo(goodsId, "有人出到" + curPrice + "了,放弃");
				goodsInfoTable.updatePaimaiStatus(goodsId,
						AuctionConst.STATUS_GIVEUP);
				return true;
			}
			goodsInfoTable.updatePaimaiStatus(goodsId,
					AuctionConst.STATUS_RUNNING);
			if (lefttime < Setting.doPriceTime()) {
				int newPrice = curPrice.intValue()
						+ GoodsContext.getInstance().addPrice(goodsId);
				if (newPrice > maxPrice) {
					newPrice = maxPrice;
				}
				if (newPrice >= 55 && newPrice <= 58) {
					newPrice = 59;
				}
				boolean success = jd.doPrice(goodsInfo, newPrice);
				goodsInfoTable
						.updateMyPrice(goodsId, Integer.valueOf(newPrice));
				goodsInfoTable
						.updateMemo(goodsId, "出价:" + newPrice + "元，祝我成功吧");

				goodsInfoTable.updatePaimaiStatus(goodsId,
						AuctionConst.STATUS_END);
				if (success) {
					notice(goodsInfo, newPrice);
				}

				return false;
			}
			return true;
		}
		return false;
	}

	private MusicPlay myMusicPlay = null;

	private void notice(GoodsInfo goodsInfo, int newPrice) {
		String title = "夺宝岛：" + goodsInfo.getGoodsName() + "-" + newPrice
				+ " 元";
		MailSenderUtil.send(title,
				"http://paimai.jd.com/" + goodsInfo.getGoodsId() + "\n from:"
						+ Setting.getUserName());
		trayicon.displayMessage(title, goodsInfo.getGoodsId(), MessageType.INFO);
		if (myMusicPlay == null) {
			myMusicPlay = new MusicPlay("/music/dog.wav");
		}
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					myMusicPlay.continuousStart();
					sleep(3000);
					myMusicPlay.continuousStop();
				} catch (Exception e) {
					Logger.error(e);
				}

			}
		});

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.addGoods) {
			onAddGoods();
		} else if (e.getSource() == this.editGoods) {
			onEditGoods();
		} else if (e.getSource() == this.removeGoods) {
			onRemoveGoods();
		} else if (e.getSource() == this.giveupGoods) {
			onGiveupGoods();
		} else if (e.getSource() == this.refreshGoods) {
			onRefreshGoods();
		} else if (e.getSource() == this.clearFinishGoods) {
			onClearFinishGoods();
		} else if (e.getSource() == auctionSetting) {
			onSetting();
		} else if (e.getSource() == batchAddGoods) {
			onBatchAddGoods();
		} else if (e.getSource() == importGoods) {
			onImportGoods();
		} else if (e.getSource() == exportUnFinishGoods) {
			onExportUnFinishGoods();
		} else if (e.getSource() == keywordManage) {
			onKeywordManage();
		}

	}

	private void onKeywordManage() {
		AutoAddGoodsDialog dialog = new AutoAddGoodsDialog(this);
		dialog.setExsitGoodsInfos(autoAddGoods);
		dialog.setVisible(true);
		if (dialog.isOk()) {
			AutoAddVO autoAddVO = new AutoAddVO(dialog.getKeyword(),
					dialog.getOffrate(), dialog.isWholeWord());
			autoAddGoods.add(autoAddVO);
			autoAddGoods(autoAddVO);
		}
	}

	private void onExportUnFinishGoods() {
		JFileChooser jfc = createFileChooser();
		if (JFileChooser.APPROVE_OPTION == jfc.showSaveDialog(this)) {
			File file = jfc.getSelectedFile();
			saveUnfinishGoods(file);
		}

	}

	private void saveUnfinishGoods(File file) {
		if (file.exists()) {
			file.delete();
		}
		if (!file.getName().toLowerCase().endsWith(".jd")) {
			file = new File(file.getAbsolutePath() + ".jd");
		}

		StringBuilder txt = new StringBuilder();
		for (GoodsInfo goodsInfo : goodsInfoTable.getGoodsInfos()) {
			if (overGoodsids.contains(goodsInfo.getGoodsId())) {
				continue;
			}
			txt.append(goodsInfo.getGoodsId());
			txt.append("-");
			txt.append(GoodsContext.getInstance().maxPrice(
					goodsInfo.getGoodsId()));
			txt.append("-");
			txt.append(GoodsContext.getInstance().addPrice(
					goodsInfo.getGoodsId()));
			txt.append(",");
		}
		if (txt.length() > 0) {
			txt.deleteCharAt(txt.length() - 1);
		}

		String content = txt.toString();
		writeFile(file, content);
	}

	private JFileChooser createFileChooser() {
		JFileChooser jfc = new JFileChooser();
		jfc.setAcceptAllFileFilterUsed(false);
		jfc.setFileFilter(new FileFilter() {

			@Override
			public String getDescription() {
				return "夺宝岛";
			}

			@Override
			public boolean accept(File f) {
				return (f.getName().endsWith(".jd"));
			}
		});
		return jfc;
	}

	private void writeFile(File file, String content) {
		FileWriter fw = null;
		try {
			fw = new FileWriter(file);
			fw.write(content);
		} catch (IOException e) {
		} finally {
			try {
				if (fw != null) {
					fw.close();
				}
			} catch (IOException e) {
			}
		}
	}

	private void onImportGoods() {
		JFileChooser jfc = createFileChooser();
		if (JFileChooser.APPROVE_OPTION == jfc.showOpenDialog(this)) {
			File file = jfc.getSelectedFile();
			loadGoods(file);
		}

	}

	public void loadGoods(File file) {
		if (!file.exists()) {
			return;
		}
		String content = readFile(file);
		String[] ids = content.split(",");
		if (ids == null || ids.length == 0) {
			return;
		}
		for (String string : ids) {
			if (string.trim().length() == 0) {
				continue;
			}
			String[] goodSettings = string.split("-");
			if (goodSettings.length < 3) {
				continue;
			}
			goodsInfoTable.addGoods(goodSettings[0],
					Integer.valueOf(goodSettings[1]),
					Integer.valueOf(goodSettings[2]));
		}
	}

	private String readFile(File file) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String line;
			StringBuilder txt = new StringBuilder();
			while ((line = br.readLine()) != null) {
				txt.append(line);
			}

			String content = txt.toString();
			return content;
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void onBatchAddGoods() {
		String keyword = JOptionPane.showInputDialog(this, "输入关键字");
		while (keyword == null || keyword.trim().length() == 0) {
			keyword = JOptionPane.showInputDialog(this, "请输入关键字");
		}
		String discount = null;
		double discount_value = 0;
		while (discount == null || discount.trim().length() == 0
				|| discount_value <= 0) {
			discount = JOptionPane.showInputDialog(this, "输入最高价折扣(0.01~~1)");
			try {
				discount_value = Double.valueOf(discount.trim());
			} catch (NumberFormatException e) {
				discount = null;
			}

		}
		List<GoodsInfo> list = UnSessionUtils.searchAuction(keyword);
		if (list.isEmpty()) {
			JOptionPane.showMessageDialog(this, "木有");
			return;
		}
		int addSize = 0;
		for (GoodsInfo goodsInfo : list) {
			if (goodsInfoTable.existGoods(goodsInfo)) {
				continue;
			}
			try {

				int maxPrice = (int) (goodsInfo.getJDPriceDouble() * discount_value);
				goodsInfoTable.addGoods(goodsInfo.getGoodsId(), maxPrice,
						Math.max(maxPrice / 100, 2));
				addSize++;
			} catch (Throwable te) {

			}
		}

		JOptionPane.showMessageDialog(this, "加了：" + addSize + "条商品");
	}

	private void onRefreshGoods() {
		for (GoodsInfo goods : goodsInfoTable.getGoodsInfos()) {
			GoodsInfo newGoodsInfo = UnSessionUtils
					.getGoods(goods.getGoodsId());
			goods.update(newGoodsInfo);
			goodsInfoTable.updateGoodsInfo(goods);
		}

	}

	private void onClearFinishGoods() {
		for (String finishId : overGoodsids) {
			goodsInfoTable.removeGoods(finishId);
		}
		overGoodsids.clear();
	}

	private void onRemoveGoods() {
		int selectRow = this.goodsInfoTable.getSelectedRow();
		if (selectRow < 0) {
			JOptionPane.showMessageDialog(this, "请选择要删除的商品");
			return;
		}
		String goodsNo = goodsInfoTable.getGoodsId(selectRow);
		overGoodsids.remove(goodsNo);
		goodsInfoTable.removeGoods(goodsNo);

	}

	private void onGiveupGoods() {
		int selectRow = this.goodsInfoTable.getSelectedRow();
		if (selectRow < 0) {
			JOptionPane.showMessageDialog(this, "请选择要放弃的商品");
			return;
		}
		String goodsNo = goodsInfoTable.getGoodsId(selectRow);
		String status = goodsInfoTable.getPaimaiStauts(selectRow);
		if (AuctionConst.STATUS_END.equals(status)) {
			JOptionPane.showMessageDialog(this, "该拍卖已经结束");
			return;
		}
		goodsInfoTable.editGoods(selectRow, goodsNo, 1, 1);

	}

	private void onSetting() {
		SettingDialog dialog = new SettingDialog(this);
		dialog.setDoPriceTime(Setting.doPriceTime());
		dialog.setRateTime(Setting.getRateTime());
		dialog.setVisible(true);
		if (dialog.isOk()) {
			Setting.setDoPriceTime(dialog.getDoPriceTime());
			Setting.setRateTime(dialog.getRateTime());
		}

	}

	private void onEditGoods() {
		int selectRow = this.goodsInfoTable.getSelectedRow();
		if (selectRow < 0) {
			JOptionPane.showMessageDialog(this, "请选择要修改的商品");
			return;
		}
		String goodsNo = goodsInfoTable.getGoodsId(selectRow);
		String status = goodsInfoTable.getPaimaiStauts(selectRow);
		if (AuctionConst.STATUS_END.equals(status)) {
			JOptionPane.showMessageDialog(this, "该拍卖已经结束");
			return;
		}
		Integer maxPrice = Integer.valueOf(GoodsContext.getInstance().maxPrice(
				goodsNo));
		Integer addPrice = Integer.valueOf(GoodsContext.getInstance().addPrice(
				goodsNo));
		GoodsDialog dialog = new GoodsDialog(this);
		dialog.setStateEdit();
		dialog.setGoodsNo(goodsNo);
		dialog.setMaxPrice(maxPrice);
		dialog.setAddPrice(addPrice);
		dialog.setVisible(true);
		if (dialog.isOk()) {
			goodsInfoTable.editGoods(selectRow, goodsNo, dialog.getMaxPrice(),
					dialog.getAddPrice());
		}
	}

	private void onAddGoods() {
		GoodsDialog dialog = new GoodsDialog(this);
		dialog.setStateAdd();
		dialog.setVisible(true);
		if (dialog.isOk())
			try {
				goodsInfoTable.addGoods(dialog.getGoodsNo(),
						dialog.getMaxPrice(), dialog.getAddPrice());
			} catch (Exception e) {
				JOptionPane.showMessageDialog(this, e.getMessage());
			}
	}

	private void autoAddGoods(AutoAddVO autoAddVO) {
		List<GoodsInfo> goodsInfos = UnSessionUtils.searchAuction(autoAddVO
				.getKeyword());
		if (goodsInfos == null) {
			return;
		}
		for (GoodsInfo goodsInfo : goodsInfos) {
			if (autoAddVO.isWholeWord()) {
				if (!autoAddVO.getKeyword().equals(goodsInfo.getGoodsName())) {
					continue;
				}
			}
			if (System.currentTimeMillis() < goodsInfo.getStartTimeMili()) {
				continue;
			}
			double price;
			if (goodsInfo.getJDPriceDouble() == null
					|| goodsInfo.getJDPriceDouble() < 0) {
				price = 2;
			} else {
				price = goodsInfo.getJDPriceDouble() * autoAddVO.getOffrate();
			}
			int addPrice = Math.max((int) (price * 0.05), 1);
			goodsInfoTable.addGoodsDirect(goodsInfo.getGoodsId(), (int) price,
					addPrice);
		}
	}
}