 package org.suigara.jd.ui;
 
 import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
 
 public class SettingDialog extends JDialog
   implements ActionListener
 {
   private static final long serialVersionUID = 1L;
 
   private JFormattedTextField rateTime = new JFormattedTextField(NumberFormat.getIntegerInstance());
 
   private JFormattedTextField doPriceTime = new JFormattedTextField(NumberFormat.getIntegerInstance());
 
   private JButton okButtion = new JButton("确定");
   private JButton cancelButtion = new JButton("取消");
 
   private boolean ok = false;
 
   public SettingDialog(JFrame f)
   {
     super(f);
 
     initUI();
     setSize(350, 160);
     setLocationRelativeTo(null);
   }
 
   private void initUI()
   {
     JPanel mainpanel = new JPanel();
     mainpanel.setLayout(new GridLayout(2, 2));
     mainpanel.add(new JLabel("扫描时间间隔（毫秒）"));
     mainpanel.add(this.rateTime);
     mainpanel.add(new JLabel("剩余多久出价（毫秒）"));
     mainpanel.add(this.doPriceTime);
 
     JPanel btnpanel = new JPanel();
     btnpanel.setLayout(new FlowLayout(1));
     btnpanel.add(this.okButtion);
     btnpanel.add(this.cancelButtion);
     this.okButtion.addActionListener(this);
     this.cancelButtion.addActionListener(this);
 
     setModal(true);
 
     setLayout(new BorderLayout());
     add(mainpanel, "Center");
     add(btnpanel, "South");
   }
 
 
 
   public void setRateTime(Long rateTime) {
     this.rateTime.setValue(rateTime);
   }
 
   public void setDoPriceTime(Long doPriceTime) {
     this.doPriceTime.setValue(doPriceTime);
   }
 
   public Long getRateTime() {
     if (this.rateTime.getValue() == null) {
       return 1000L;
     }
     return Long.valueOf(((Number)this.rateTime.getValue()).longValue());
   }
 
   public Long getDoPriceTime() {
     if (this.doPriceTime.getValue() == null) {
       return null;
     }
     return Long.valueOf(((Number)this.doPriceTime.getValue()).longValue());
   }
 
   public boolean isOk()
   {
     return this.ok;
   }
 
   public void actionPerformed(ActionEvent e)
   {
     if (e.getSource() == this.okButtion) {
       if ((getRateTime() == null) || (getRateTime().intValue() < 500)) {
         JOptionPane.showMessageDialog(this, "请输入合法的扫描间隔(>500)");
         return;
       }
       if ((getDoPriceTime() == null) || (getDoPriceTime().intValue() < 1)) {
         JOptionPane.showMessageDialog(this, "请输入剩余多久出价(最好在10000毫秒以内，推荐4000)");
         return;
       }
       this.ok = true;
     } else {
       this.ok = false;
     }
     setVisible(false);
   }
 }
