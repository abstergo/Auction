package org.suigara.jd.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.jsoup.helper.StringUtil;
import org.suigara.jd.bean.AutoAddVO;

public class AutoAddGoodsDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;

	private JTextArea textArea = new JTextArea(3, 25);

	private JTextField keywordText = new JTextField();

	private JFormattedTextField offrateText = new JFormattedTextField(
			NumberFormat.getNumberInstance());

	private JCheckBox wholeWordBox = new JCheckBox("是否全匹配");

	private JButton okButtion = new JButton("确定");
	private JButton cancelButtion = new JButton("取消");

	private boolean ok = false;

	public AutoAddGoodsDialog(JFrame f) {
		super(f);

		initUI();
		setSize(400, 200);
		setLocationRelativeTo(null);
	}

	public void setExsitGoodsInfos(List<AutoAddVO> autoAddGoods) {
		textArea.setText("");
		for (AutoAddVO autoAddVO : autoAddGoods) {
			textArea.append(autoAddVO.getKeyword() + ","
					+ (autoAddVO.getOffrate() * 100) + "%" + ","
					+ autoAddVO.isWholeWord());
			textArea.append("\n");
		}
	}

	private void initUI() {
		JPanel mainpanel = new JPanel();
		mainpanel.setLayout(new GridLayout(4, 2));

		textArea.setEditable(false);

		mainpanel.add(new JLabel("已有"));
		mainpanel.add(this.textArea);
		mainpanel.add(new JLabel("关键字"));
		mainpanel.add(this.keywordText);
		mainpanel.add(new JLabel("折扣"));
		mainpanel.add(this.offrateText);
		mainpanel.add(new JLabel(""));
		mainpanel.add(this.wholeWordBox);

		JPanel btnpanel = new JPanel();
		btnpanel.setLayout(new FlowLayout(1));
		btnpanel.add(this.okButtion);
		btnpanel.add(this.cancelButtion);
		this.okButtion.addActionListener(this);
		this.cancelButtion.addActionListener(this);

		setModal(true);

		setLayout(new BorderLayout());
		add(mainpanel, "Center");
		add(btnpanel, "South");
	}

	public String getKeyword() {
		return this.keywordText.getText().trim();
	}

	public Double getOffrate() {
		if (this.offrateText.getValue() == null) {
			return null;
		}
		return Double.valueOf(this.offrateText.getValue().toString());
	}

	public boolean isWholeWord() {
		return wholeWordBox.isSelected();
	}

	public boolean isOk() {
		return this.ok;
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.okButtion) {
			if (StringUtil.isBlank(getKeyword())) {
				JOptionPane.showMessageDialog(this, "关键字");
				return;
			}
			if (getOffrate() == null || getOffrate() <= 0 || getOffrate() >= 1) {
				JOptionPane.showMessageDialog(this, "请输入合法的折扣");
				return;
			}
			this.ok = true;
		} else {
			this.ok = false;
		}
		setVisible(false);
	}
}
