package org.suigara.jd.ui;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.suigara.jd.log.Logger;
import org.suigara.jd.utils.DigestUtils;

public class Login {
	private static final File FILE = new File("login.properties");
	/**
	 * 
	 */
	public static JTextField usernameField = null;
	public static JPasswordField passwordField = null;
	private static JCheckBox autoLoginBox = null;

	public static int showLoginGUI(JFrame parent) {
		usernameField = new JTextField(24);
		passwordField = new JPasswordField(24);
		autoLoginBox = new JCheckBox("自动登录");

		setUserNameAndPassword();

		if (autoLoginBox.isSelected()) {
			boolean hasUserName = userName() != null && userName().length() > 0;
			boolean hasPassword = password() != null && password().length() > 0;
			if (hasUserName && hasPassword) {
				return 0;
			}
		}

		Object[] options = { "登录", "取消" };
		int i = JOptionPane.showOptionDialog(parent, createLoginPanel(),
				"登录信息", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
				null, options, options[0]);
		if (i == 0) {
			saveLoginInfo(userName(), password(), autoLoginBox.isSelected());
		}
		return i;
	}

	@SuppressWarnings("deprecation")
	private static String password() {
		return passwordField.getText();
	}

	private static String userName() {
		return usernameField.getText();
	}

	private static JPanel createLoginPanel() {
		JPanel ret = new JPanel();

		JPanel usernamePanel = new JPanel();
		usernamePanel.add(new JLabel("用户名：", JLabel.RIGHT));
		usernamePanel.add(usernameField);
		JPanel passwordPanel = new JPanel();
		passwordPanel.add(new JLabel("密     码：", JLabel.RIGHT));
		passwordPanel.add(passwordField);

		Box box = new Box(BoxLayout.Y_AXIS);
		box.add(usernamePanel);
		box.add(passwordPanel);
		box.add(autoLoginBox);

		ret.add(box);

		ret.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createLineBorder(new Color(244, 144, 44)),
				"填写登录信息"));
		return ret;
	}

	private static void setUserNameAndPassword() {
		if(!FILE.exists()){
			return;
		}
		Properties prop = new Properties();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(FILE);
			System.out.println(FILE.getAbsolutePath());
			prop.load(fis);
			String username = prop.getProperty("username");
			String password = prop.getProperty("password");
			System.out.println("password="+password);
			boolean autoLogin = Boolean.valueOf(prop.getProperty("autologin"));
			usernameField.setText(username);
			String passwordDecode = "";
			if (password != null && !password.isEmpty()) {
				passwordDecode = DigestUtils.decrypt(password);
			}
			passwordField.setText(passwordDecode);

			autoLoginBox.setSelected(autoLogin);
		} catch (IOException e) {
		} finally{
			if(fis!=null){
				try {
					fis.close();
				} catch (IOException e) {
				}
			}
		}
	}

	public static void saveLoginInfo(String userName, String password,
			boolean autoLogin) {
		Properties prop = new Properties();
		FileOutputStream oFile = null;
		try {
			oFile = new FileOutputStream(FILE);
			prop.put("username", userName);
			prop.put("password", DigestUtils.encrypt(password));
			prop.put("autologin", String.valueOf(autoLogin));
			prop.store(oFile, "login info");
		} catch (IOException e) {
			Logger.error(e);
		} finally{
			if(oFile!=null){
				try {
					oFile.close();
				} catch (IOException e) {
				}
			}
		}

	}

}