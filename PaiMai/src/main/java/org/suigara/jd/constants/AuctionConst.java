package org.suigara.jd.constants;

public class AuctionConst
{
  public static final String STATUS_RUNNING = "进行中";
  public static final String STATUS_END = "已结束";
  public static final String STATUS_GIVEUP = "已放弃";
  public static final String STATUS_NOSTART = "未开始";
}